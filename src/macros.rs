#[macro_export]
macro_rules! mat {
    ($($($x: expr),*);*) => {
        crate::Matrix::new([$([$({$x},)*],)*])
    }
}

#[cfg(test)]
mod tests {
    use crate::Matrix;

    #[test]
    fn mat_macro() {
        assert_eq!(
            crate::mat![3, 5, 7; 2, 5, 8],
            Matrix::new([[3, 5, 7], [2, 5, 8]])
        );
    }
}
