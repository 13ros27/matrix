use std::convert::TryInto;

mod macros;
mod operations;

#[derive(Clone, Debug, PartialEq)]
pub struct Matrix<T, const ROW_SIZE: usize, const COLUMN_SIZE: usize> {
    pub(crate) contents: [[T; ROW_SIZE]; COLUMN_SIZE],
}

impl<T, const ROW_SIZE: usize, const COLUMN_SIZE: usize> Matrix<T, ROW_SIZE, COLUMN_SIZE> {
    pub fn new(contents: [[T; ROW_SIZE]; COLUMN_SIZE]) -> Self {
        Self { contents }
    }

    pub const fn is_empty(&self) -> bool {
        self.contents.is_empty()
    }

    pub const fn len(&self) -> (usize, usize) {
        (self.contents[0].len(), self.contents.len())
    }

    pub const fn is_square(&self) -> bool {
        self.is_empty() || self.len().0 == self.len().1
    }
}

impl<T, const ROW_SIZE: usize> Matrix<T, ROW_SIZE, ROW_SIZE>
where
    T: num::Num + std::fmt::Debug,
{
    pub fn identity() -> Self {
        Self::new(
            (0..ROW_SIZE)
                .map(|y| {
                    (0..ROW_SIZE)
                        .map(|x| if x == y { T::one() } else { T::zero() })
                        .collect::<Vec<_>>()
                        .try_into()
                        .unwrap()
                })
                .collect::<Vec<_>>()
                .try_into()
                .unwrap(),
        )
    }
}
