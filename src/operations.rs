use crate::Matrix;
use std::convert::TryInto;
use std::ops::{Add, Index, Sub};

impl<T, const ROW_SIZE: usize, const COLUMN_SIZE: usize> Index<(usize, usize)>
    for Matrix<T, ROW_SIZE, COLUMN_SIZE>
{
    type Output = T;
    fn index(&self, index: (usize, usize)) -> &T {
        &self.contents[index.0][index.1]
    }
}

impl<T, const ROW_SIZE: usize, const COLUMN_SIZE: usize> Index<usize>
    for Matrix<T, ROW_SIZE, COLUMN_SIZE>
{
    type Output = [T; ROW_SIZE];
    fn index(&self, index: usize) -> &[T; ROW_SIZE] {
        &self.contents[index]
    }
}

impl<
        T: Add<Output = T> + std::fmt::Debug + Copy,
        const ROW_SIZE: usize,
        const COLUMN_SIZE: usize,
    > Add for Matrix<T, ROW_SIZE, COLUMN_SIZE>
{
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        Self::new(
            (0..ROW_SIZE)
                .map(|y| {
                    (0..ROW_SIZE)
                        .map(|x| self[x][y] + rhs[x][y])
                        .collect::<Vec<_>>()
                        .try_into()
                        .unwrap()
                })
                .collect::<Vec<_>>()
                .try_into()
                .unwrap(),
        )
    }
}

impl<
        T: Sub<Output = T> + std::fmt::Debug + Copy,
        const ROW_SIZE: usize,
        const COLUMN_SIZE: usize,
    > Sub for Matrix<T, ROW_SIZE, COLUMN_SIZE>
{
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        Self::new(
            (0..ROW_SIZE)
                .map(|y| {
                    (0..ROW_SIZE)
                        .map(|x| self[x][y] - rhs[x][y])
                        .collect::<Vec<_>>()
                        .try_into()
                        .unwrap()
                })
                .collect::<Vec<_>>()
                .try_into()
                .unwrap(),
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::mat;

    #[test]
    fn compare_index() {
        let matrix = mat![3, 5, 7; 2, 5, 8];
        assert_eq!(matrix[(1, 0)], matrix[1][0]);
        assert_eq!(matrix[(1, 2)], matrix[1][2]);
        assert_eq!(matrix[(0, 1)], matrix[0][1]);
    }
}
